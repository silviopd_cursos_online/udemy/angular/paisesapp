import {Component, OnInit} from '@angular/core';
import {PaisService} from "../../services/pais.service";
import {Country} from "../../interface/pais";

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styleUrls: ['./por-pais.component.css']
})
export class PorPaisComponent implements OnInit {

  termino: string = '';
  hayError: boolean = false;
  paises: Country[] = [];
  paisesSgugeridos: Country[] = [];
  showSuggestion: boolean = false;

  constructor(private paisService: PaisService) {
  }

  ngOnInit(): void {
  }

  buscar = (search: string) => {
    this.showSuggestion = false
    this.termino = search;
    this.hayError = false;
    this.paisService.buscarPais(this.termino).subscribe({
      next: (data) => {
        console.log(data);
        this.paises = data;
      },
      error: (err) => {
        console.log(err);
        this.hayError = true;
        this.paises = [];
        this.showSuggestion = false
      },
      complete: () => {
        this.showSuggestion = false
      }
    })
  }

  sugerencias = (event: string) => {
    this.paises = [];
    this.termino = event

    if (event.length == 0) {
      this.paisesSgugeridos = [];
      return
    }

    this.hayError = false;
    console.log(event);

    this.paisService.buscarPais(event).subscribe({
      next: (data) => {
        console.log(data);
        this.showSuggestion = this.paises.length <= 0;
        this.paisesSgugeridos = data.splice(0, 5);
      },
      error: (err) => {
        console.log(err);
        this.paisesSgugeridos = [];
        this.showSuggestion = false
      }
    })
  }
}
