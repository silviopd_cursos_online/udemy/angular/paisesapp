import {Component, OnInit} from '@angular/core';
import {PaisService} from "../../services/pais.service";
import {Country} from "../../interface/pais";

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styleUrls: ['./por-region.component.css']
})
export class PorRegionComponent implements OnInit {

  regiones: string[] = ['africa', 'americas', 'asia', 'europe', 'oceania'];
  regionActive: string = '';
  paises: Country[] = [];

  constructor(private paisService: PaisService) {
  }

  ngOnInit(): void {
  }

  onRegionChange(region: string) {

    if (region === this.regionActive) {
      return
    }

    this.paises = [];
    this.regionActive = region;
    this.paisService.buscarRegion(region).subscribe({
      next: (paises) => {
        console.log(paises);
        this.paises = paises;
      },
      error: (err) => {
        console.log(err);
        this.paises = [];
      }
    });
  }

  getClassRegionActive(region: string) {
    return region === this.regionActive ? 'btn btn-primary' : 'btn btn-outline-primary'
  }
}
