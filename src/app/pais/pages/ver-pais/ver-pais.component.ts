import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PaisService} from "../../services/pais.service";
import {switchMap, tap} from "rxjs";
import {Country} from "../../interface/pais";

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styleUrls: ['./ver-pais.component.css']
})
export class VerPaisComponent implements OnInit {

  pais!: Country;

  constructor(private activatedRoute: ActivatedRoute, private paisService: PaisService, private router: Router) {

  }

  ngOnInit(): void {

    this.activatedRoute.params
      .pipe(
        switchMap(({id}) => this.paisService.buscarCodigo(id)),
        tap(console.log)
      )
      .subscribe({
        next: (resp) => {
          this.pais = resp[0];
          // console.log(resp);
        },
        error: (err) => {
          console.log(err);
          this.router.navigate(['/']);
        }
      })

  }

}
