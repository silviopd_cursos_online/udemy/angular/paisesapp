import {Component, OnInit} from '@angular/core';
import {Country} from "../../interface/pais";
import {PaisService} from "../../services/pais.service";

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styleUrls: ['./por-capital.component.css']
})
export class PorCapitalComponent implements OnInit {

  termino: string = '';
  hayError: boolean = false;
  capital: Country[] = [];
  capitalSgugeridos: Country[] = [];
  showSuggestion: boolean = false;

  constructor(private paisService: PaisService) {
  }

  ngOnInit(): void {
  }

  buscar = (search: string) => {
    this.showSuggestion = false;
    this.termino = search;
    this.hayError = false;
    this.paisService.buscarCapital(this.termino).subscribe({
      next: (data) => {
        console.log(data);
        this.capital = data;
      },
      error: (err) => {
        console.log(err);
        this.hayError = true;
        this.capital = [];
        this.showSuggestion = false
      },
      complete: () => {
        this.showSuggestion = false
      }
    })
  }

  sugerencias = (event: string) => {
    this.capital = [];
    this.termino = event

    if (event.length == 0) {
      this.capitalSgugeridos = [];
      return
    }

    this.hayError = false;
    console.log(event);

    this.paisService.buscarCapital(event).subscribe({
      next: (data) => {
        console.log(data);
        this.showSuggestion = this.capital.length <= 0;
        this.capitalSgugeridos = data.splice(0, 5);
      },
      error: (err) => {
        console.log(err);
        this.capital = [];
        this.showSuggestion = false
      }
    })
  }
}
