import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Country} from "../interface/pais";

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private apiUrl: string = 'https://restcountries.com/v3.1/';

  get httParams() {
    return new HttpParams().set('fields', 'flags,name,capital,population,cca2');
  }

  constructor(private http: HttpClient) {
  }

  buscarPais(termino: string): Observable<Country[]> {
    return this.http.get<Country[]>(`${this.apiUrl}/name/${termino}`, {params: this.httParams});
  }

  buscarCapital(termino: string): Observable<Country[]> {
    return this.http.get<Country[]>(`${this.apiUrl}/capital/${termino}`, {params: this.httParams});
  }

  buscarCodigo(termino: string): Observable<Country> {
    return this.http.get<Country>(`${this.apiUrl}/alpha?codes=${termino}`);
  }

  buscarRegion(termino: string): Observable<Country[]> {
    return this.http.get<Country[]>(`${this.apiUrl}/region/${termino}`, {params: this.httParams});
  }
}
